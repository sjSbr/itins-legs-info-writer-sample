package com.sabre.sample;


import com.sabre.fltsked.extensions.modules.profit.ds.Frequency;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.FlightLeg;
import com.sabre.fltsked.extensions.modules.profit.ds.connectionbuilder.Itinerary;

import java.util.ArrayList;
import java.util.List;

import static com.sabre.sample.InfoRWUtil.CLOSE_BRACKET;
import static com.sabre.sample.InfoRWUtil.COMMA;
import static com.sabre.sample.InfoRWUtil.OPEN_BRACKET;

public class ItinXformUtil
{
    private final FrequencyXformUtil freqXformer = new FrequencyXformUtil();
    private final LegXformUtil legXformer = new LegXformUtil();

    String getHeader()
    {
        return "Dept Airport, Arrival Airport, Frequency, Dept Time, Arr Time, Elapsed Time, Distance, No Of Legs, [LEGINFO: " + legXformer.getHeader() + "]...";
    }

    public String xToString(Itinerary itin)
    {
        final StringBuilder itinStr = new StringBuilder();
        itinStr.append(itin.getDeptArp()).append(COMMA);
        itinStr.append(itin.getArvlArp()).append(COMMA);
        itinStr.append(freqXformer.xToString(itin.getFrequency())).append(COMMA);
        itinStr.append(String.valueOf(itin.getDeptTime())).append(COMMA);
        itinStr.append(String.valueOf(itin.getArvlTime())).append(COMMA);
        itinStr.append(itin.getElapsedTime()).append(COMMA);
        itinStr.append(itin.getDistance()).append(COMMA);

        final List<FlightLeg> legList = itin.getLegs();
        itinStr.append(legList.size()).append(COMMA);
        for (FlightLeg leg : legList)
        {
            itinStr.append(OPEN_BRACKET).append(legXformer.xToString(leg)).append(CLOSE_BRACKET);
        }

        return itinStr.toString();
    }

    public Itinerary parseItin(String itinInfo)
    {
        final int legInfoIdx = itinInfo.indexOf(InfoRWUtil.OPEN_BRACKET);
        final String[] itinStr = itinInfo.substring(0, legInfoIdx - 1).split(InfoRWUtil.COMMA);
        final String deptArp = itinStr[0];
        final String arrArp = itinStr[1];
        final Frequency frequency = freqXformer.parseFrequency(itinStr[2]);
        final int deptTime = Integer.parseInt(itinStr[3]);
        final int arvTime = Integer.parseInt(itinStr[4]);
        final int elapsedTime = Integer.parseInt(itinStr[5]);
        final int distance = Integer.parseInt(itinStr[6]);
        final int noOfLegs = Integer.parseInt(itinStr[7]);
        List<FlightLeg> legs = getLegs(noOfLegs, itinInfo.substring(legInfoIdx, itinInfo.length()));
        return new Itin(deptArp, arrArp, deptTime, arvTime, elapsedTime, distance, frequency, legs);
    }

    private List<FlightLeg> getLegs(int noOfLegs, String legInfo)
    {
        final ArrayList<FlightLeg> legs = new ArrayList<FlightLeg>();
        String legStr = legInfo;
        for (int legIdx = 0; legIdx < noOfLegs; legIdx++)
        {
            int open = legStr.indexOf(InfoRWUtil.OPEN_BRACKET);
            int close = legStr.indexOf(InfoRWUtil.CLOSE_BRACKET);

            legs.add(legXformer.parseLeg(legStr.substring(open + 1, close)));

            legStr = legStr.substring(close + 1);
        }
        return legs;
    }

}
